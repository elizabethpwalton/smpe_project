# Estimating Color Names by Web Image Searches

This is a project by Dominik Schreiber and Elizabeth Walton done as a part of the *Scientific Methodology and Performance Evaluation* course at Grenoble ENSIMAG during the winter semester of 2017/18.

Please refer to the Jupyter notebook or for the pure, non-interactive html version `Notebook.slides.html` in order to read more about the project.
